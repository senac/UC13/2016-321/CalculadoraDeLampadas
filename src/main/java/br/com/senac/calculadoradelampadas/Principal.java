
package br.com.senac.calculadoradelampadas;

import java.util.Scanner;


public class Principal {
    
    public static void main(String... a){
        
        Comodo comodo ; 
        
        double largura ; 
        double comprimento ; 
        double potencia ; 
        int quantidadeLampadas ;
        
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.println("Digite a Largura:");
        largura = scanner.nextDouble(); 
        
        System.out.println("Digite a Comprimento:");
        comprimento = scanner.nextDouble() ; 
        
        System.out.println("Digite a Potencia da Lampada:");
        potencia = scanner.nextDouble() ; 
        
        comodo = new Comodo(largura, comprimento);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas(); 
        quantidadeLampadas = calculadoraDeLampadas.Cacular(potencia, comodo);
        
        System.out.println("Serao necessarias :" + quantidadeLampadas);
        
        
        
    }
    
}
