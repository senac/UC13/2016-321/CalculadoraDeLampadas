package br.com.senac.calculadoradelampadas;

public class Comodo {

    private double largura;
    private double comprimento;

    public Comodo() {
    }

    public Comodo(double largura, double comprimento) {
        this.largura = largura;
        this.comprimento = comprimento;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getComprimento() {
        return comprimento;
    }

    public void setComprimento(double comprimento) {
        this.comprimento = comprimento;
    }
    
    public double getArea(){
        return this.comprimento * this.largura;
    }

}
